# Clip example

This example demonstrates how to use the [Moku Library](https://compile.liquidinstruments.com/docs/support.html) to clip the input signal to a defined bit range.
