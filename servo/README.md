# Servo Driver example

This [Servo Driver](https://compile.liquidinstruments.com/docs/examples/servo.html) module takes an analog input (perhaps from a PID Controller in an adjacent slot) and converts it to a 50Hz pulse train, suitable for position control of common hobby servo motors.
