# Register Gate example

This example allows the [Control Registers](https://compile.liquidinstruments.com/docs/examples/#control-registers) to enable and disable the Outputs, creating gated behaviour.

| Control1 | Output A | Output B |
| --- | --- | --- |
| 00 |	Off |	Off |
| 01 |	Input A |	Off |
| 10 |	Off |	Input B |
| 11 |	Input A |	Input B |
