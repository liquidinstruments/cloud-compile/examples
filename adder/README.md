# Adder example

This is a simple addition and subtraction example. Simply add two inputs and route to an output, subtract two inputs and route to another output.
- Output A is Input A + Input B;
- Output B is Input A – Input B.
